use azure_sdk_core;

use self::common::{DebugSetting, DeploymentMode, OnErrorDeploymentType, Parameters, Template};

mod common;
mod request;
mod response;

const URI: &str = "https://management.azure.com/subscriptions/{subscriptionId}/resourcegroups/{resourceGroupName}/providers/Microsoft.Resources/deployments/{deploymentName}?api-version=2019-10-01";

#[derive(Debug)]
pub struct Client;

#[derive(Debug)]
pub struct Deployment;

impl Deployment {
    const API_VERSION: &'static str = "2019-10-01";

    fn new() -> Self {
        Self
    }

    fn api_version(&self) -> &str {
        Self::API_VERSION
    }
}
