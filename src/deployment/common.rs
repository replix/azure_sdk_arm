use std::collections::HashMap;

use derivative::Derivative;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DebugSetting {
    /// Specifies the type of information to log for debugging.
    /// The permitted values are none, requestContent, responseContent,
    /// or both requestContent and responseContent separated by a comma.
    /// The default is none. When setting this value, carefully consider
    /// the type of information you are passing in during deployment.
    /// By logging information about the request or response,
    /// you could potentially expose sensitive data that is
    /// retrieved through the deployment operations.
    detail_level: String,
}

impl Default for DebugSetting {
    fn default() -> Self {
        Self {
            detail_level: String::from("none"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum DeploymentMode {
    Complete,
    Incremental,
}

/// Use either the parametersLink property or the parameters property, but not both.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Parameters {
    /// Name and value pairs that define the deployment parameters for the template.
    /// You use this element when you want to provide the parameter values directly
    /// in the request rather than link to an existing parameter file.
    /// Use either the parametersLink property or the parameters property,
    /// but not both. It can be a JObject or a well formed JSON string.
    Parameters(HashMap<String, String>),
    /// The URI of parameters file. You use this element to link to an existing parameters file.
    ParametersLink { uri: String },
}

///  Use either the templateLink property or the template property, but not both.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Template {
    /// The template content. You use this element when you want to pass the template
    /// syntax directly in the request rather than link to an existing template.
    /// It can be a JObject or well-formed JSON string.
    /// Use either the templateLink property or the template property, but not both.
    Template(HashMap<String, String>),
    /// The URI of the template.
    TemplateLink { uri: String },
}

#[derive(Debug, Derivative, Serialize, Deserialize)]
#[derivative(Default)]
pub(crate) enum OnErrorDeploymentType {
    #[derivative(Default)]
    LastSuccessful,
    SpecificDeployment,
}
