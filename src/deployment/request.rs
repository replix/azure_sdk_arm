use serde::{Deserialize, Serialize};

use super::{DebugSetting, DeploymentMode, OnErrorDeploymentType, Parameters, Template};

const API_VERSION: &str = "2019-10-01";

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeploymentProperties {
    /// The debug setting of the deployment.
    debug_settings: DebugSetting,
    /// The mode that is used to deploy resources.
    /// This value can be either Incremental or Complete.
    /// In Incremental mode, resources are deployed without
    /// deleting existing resources that are not included in the template.
    /// In Complete mode, resources are deployed and existing resources in
    /// the resource group that are not included in the template are deleted.
    /// Be careful when using Complete mode as you may unintentionally delete resources.
    mode: DeploymentMode,
    /// The deployment on error behavior.
    on_error_deployment: OnErrorDeployment,
    parameters: Parameters,
    template: Template,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OnErrorDeployment {
    deployment_name: String,
    r#type: OnErrorDeploymentType,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
