use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use super::{DebugSetting, DeploymentMode, OnErrorDeploymentType, Parameters, Template};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeploymentExtended {
    id: String,
    #[serde(default)]
    location: String,
    name: String,
    properties: DeploymentPropertiesExtended,
    r#type: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeploymentPropertiesExtended {
    correlation_id: String,
    #[serde(default)]
    debug_setting: DebugSetting,
    dependencies: Vec<Dependency>,
    duration: String,
    mode: DeploymentMode,
    on_error_deployment: OnErrorDeploymentExtended,
    #[serde(default)]
    outputs: HashMap<String, String>,
    #[serde(flatten)]
    parameters: Parameters,
    providers: Vec<Provider>,
    provisioning_state: String,
    #[serde(flatten)]
    template: Template,
    timestamp: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Dependency {
    depends_on: Vec<BasicDependency>,
    id: String,
    resource_name: String,
    resource_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BasicDependency {
    id: String,
    resource_name: String,
    resource_type: String,
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default, rename_all = "camelCase")]
pub struct OnErrorDeploymentExtended {
    deployment_name: String,
    provisioning_state: String,
    r#type: OnErrorDeploymentType,
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default, rename_all = "camelCase")]
pub struct Provider {
    id: String,
    namespace: String,
    registration_policy: String,
    registration_state: String,
    resource_types: Vec<ProviderResourceType>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default, rename_all = "camelCase")]
pub struct ProviderResourceType {
    aliases: Vec<AliasType>,
    api_versions: Vec<String>,
    capabilities: String,
    locations: Vec<String>,
    properties: HashMap<String, String>,
    resource_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AliasType {
    name: String,
    paths: Vec<AliasPathType>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AliasPathType {
    api_versions: Vec<String>,
    path: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn redeploy_200() -> Result<(), Box<dyn std::error::Error>> {
        let response = include_str!("redeploy_response_200.json");
        let deployment: DeploymentExtended = serde_json::from_str(response)?;
        Ok(())
    }
}
